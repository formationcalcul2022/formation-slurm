#! /usr/bin/env bash

# PARAMETERS

#SBATCH -J array                                       # JOB NAME
#SBATCH -A inrae-infra                                 # GROUP/ACCOUNT NAME
#SBATCH -p defq                                        # PARTITION NAME
#SBATCH --output=/lustre/%u/logs/%x-%A-%a.out          # OUTPUT FILE
#SBATCH --error=/lustre/%u/logs/%x-%A-%a.err           # ERROR FILE
#SBATCH --mail-type=ALL                                # STATUS MAILED (BEGIN, FAILED, END)
#SBATCH --mail-user=jeremy.verrier@inrae.fr            # USER EMAIL  
#SBATCH --mem=1G                                       # MEMORY NEEDED FOR THE JOB
#SBATCH -c 1                                           # NUMBER OF CORES FOR THE JOB
#SBATCH --time=01:00:00                                # TIME LIMIT

#SBATCH --array=1-10

# MY COMMAND HERE =>

WORK_DIR=/lustre/$USER/R-ARRAY
INPUT_DIR=/lustre/$USER/R-ARRAY/inputs

# Going to work dir
cd /lustre/$USER/R-ARRAY

# Loading R module
module load R

# Get "nth" file corresponding to ${SLURM_ARRAY_TASK_ID} with sed
R_FILE=$(ls -1 ${INPUT_DIR} | sed -n ${SLURM_ARRAY_TASK_ID}p)

# Launching arrays
Rscript array.R ${R_FILE}