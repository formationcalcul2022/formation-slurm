# SLURM ARRAY

From: https://hpc-unibe-ch.github.io/slurm/array-jobs.html 

Array jobs are jobs where the job setup, including job size, memory, time etc. is constant, but the application input varies. One use case are parameter studies.

Instead of submitting N jobs independently, you can submit one array job unifying N tasks. These provide advantages in the job handling as well as for the SLURM scheduler.

## Submitting an Array

To submit an array job, specify the number of tasks as a range of task IDs using the –array option:
```bash
#SBATCH --array=n[,k[,...]][-m[:s]]%<max_tasks>
```

The task id range specified in the option argument may be:
* comma separated list of values:
```bash
#SBATCH --array=1,3,5
```
* simple range of the form n-m:
```bash
#SBATCH --array=201-300 (201, 202, 203, …, 300)
```
* range with a step size s:
```bash
#SBATCH --array=100-200:2 (100, 102, 104, … 200)
```
* combination thereof:
```bash
#SBATCH --array=1,3,100-200 (1, 3, 100, 101, 102, …, 200)
```

Furthermore, the amount of concurent running jobs can limited using the % seperator, e.g. for max 100 concurrent jobs of 1-400:
```bash
#SBATCH --array=1-400%100
```
Therewith you can prevent fully filling your available resources. The task IDs will be exported to the job tasks via the environment variable **SLURM_ARRAY_TASK_ID**.

Additionally, SLURM_ARRAY_TASK_MAX, SLURM_ARRAY_TASK_MIN, SLURM_ARRAY_TASK_STEP are available in job, describing the task range of the job.

Specifying --array=10 will not submit an array job with 10 tasks, but an array job with a single task with task id 10. To run an array job with multiple tasks you must specify a range or a comma separated list of task ids.

## Output files

Per default the output files are named as slurm-<jobid>_<taskid>.out. When renaming the output/error files variables for the job ID (%A) and for the task ID (%a) can be used. For example:

```bash
#SBATCH --output=array_example_%A_%a.out
#SBATCH --error=array_example_%A_%a.err
```

Thus a file array_example_6543212_12.out will be written for the 12th task of job 6543212.

## Canceling Individual Tasks

You can cancel individual tasks of an array job by indicating tasks ids to the scancel command:

```bash
$ squeue
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
79265_[49-99:2%20]      test Simple H  foo      PD      0:00      1 (QOSMaxCpuPerUserLimit)
          79265_41      test Simple H  foo      R       0:10      1 fnode03
          79265_43      test Simple H  foo      R       0:10      1 fnode03
          79265_45      test Simple H  foo      R       0:10      1 fnode03
          79265_47      test Simple H  foo      R       0:10      1 fnode03
```

Use the --array option to the squeue command to display one tasks per line:

```bash
$ squeue --array
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
          79265_65      test Simple H  foo      PD      0:00      1 (QOSMaxCpuPerUserLimit)
          79265_67      test Simple H  foo      PD      0:00      1 (QOSMaxCpuPerUserLimit)
          79265_69      test Simple H  foo      PD      0:00      1 (QOSMaxCpuPerUserLimit)
          79265_97      test Simple H  foo      PD      0:00      1 (QOSMaxCpuPerUserLimit)
          79265_57      test Simple H  foo      R       0:47      1 fnode03
          79265_59      test Simple H  foo      R       0:47      1 fnode03
          79265_61      test Simple H  foo      R       0:47      1 fnode03
          79265_63      test Simple H  foo      R       0:47      1 fnode03
```

## Examples
### Use case 1: 1000 computations, same resource requirements, different input/output arguments

Instead of submitting 1000 individual jobs, submit a single array jobs with 1000 tasks:

```bash
#!/bin/bash
#SBATCH --time=00:30:00     # Each task takes max 30 minutes
#SBATCH --mem-per-cpu=2G    # Each task uses max 2G of memory
#SBATCH --array=1-1000      # Submit 1000 tasks with task ID 1,2,...,1000.

# The name of the input files must reflect the task ID!

srun ./foo input_data_${SLURM_ARRAY_TASK_ID}.txt > output_${SLURM_ARRAY_TASK_ID}.txt
```

Task with ID 20 will run the program foo with the following arguments:
```bash
./foo input_data_20.txt > output_20.txt
```

### Use case 2: Read arguments from file

Submit an array job with 1000 tasks. Each task executes the program foo with different arguments:

```bash
#!/bin/bash
#SBATCH --time=00:30:00    # Each task takes max 30 minutes
#SBATCH --mem-per-cpu=2G   # Each task uses max 2G of memory
### Submit 1000 tasks with task ID 1,2,...,1000. Run max 20 tasks concurrently
#SBATCH --array=1-1000%20  

data_dir=$WORKSPACE/projects/example/input_data        
result_dir=$WORKSPACE/projects/example/results

param_store=$WORKSPACE/projects/example/args.txt     
### args.txt contains 1000 lines with 2 arguments per line.
###    Line <i> contains arguments for run <i>
# Get first argument
param_a=$(cat $param_store | awk -v var=$SLURM_ARRAY_TASK_ID 'NR==var {print $1}')
# Get second argument
param_b=$(cat $param_store | awk -v var=$SLURM_ARRAY_TASK_ID 'NR==var {print $2}')

### Input files are named input_run_0001.txt,...input_run_1000.txt
###    Zero pad the task ID to match the numbering of the input files
n=$(printf "%04d" $SLURM_ARRAY_TASK_ID)


srun ./foo -c $param_a -p $param_b -i ${data_dir}/input_run_${n}.txt -o ${result_dir}
```

## Exercise
Using files [array.R](./array.R), create a batch file to submit an array ranging from 1 to 10 to display the name of the files in the [input folder](./inputs/).

To launch the R script, you will have to do the following tasks =>
```bash
# Creating R-ARRAY workspace in lustre
cd /lustre/$USER/
mkdir R-ARRAY

# Download R-ARRAY
wget https://forgemia.inra.fr/formationcalcul2022/formation-slurm/-/raw/main/Exercise8-SLURM_ARRAY/array.R

# Loading R module
module load R
# Testing in your terminal ... 
Rscript array.R test

# Should print
# [1] "My file is a character named test"
```

To download the input folders, follow this [link](https://forgemia.inra.fr/formationcalcul2022/formation-slurm/-/tree/main/Exercise8-SLURM_ARRAY/inputs) and the screen capture below =>
![download-folder-gitlab.png](./download-folder-gitlab.png?raw=true)

Upload the "tar.gz" file to the cluster in your "/lustre/$USER/R-ARRAY" folder via MobaXterm or rsync, scp ...

Uncompress it by typing =>
```bash
cd /lustre/$USER/R-ARRAY

# --strip-components to get a clean "inputs" folder
tar --strip-components=2 -xvzf formation-slurm-main-Exercise8-SLURM_ARRAY-inputs.tar.gz

# Cleaning directory ...
rm formation-slurm-main-Exercise8-SLURM_ARRAY-inputs.tar.gz 
```

Use the [pi.sh](../Exercise5-Computing_PI/pi.sh) file to get started.

Here is a list of what you have to do =>
1. change --job-name
2. change outputs and errors files to add the TASK-ID "%a"
3. add 
```bash
--array=1-10
```
4. add all command lines needed to launch Rscript with a different file each time =>
```bash
# MY COMMAND HERE =>

WORK_DIR=/lustre/$USER/R-ARRAY
INPUT_DIR=/lustre/$USER/R-ARRAY/inputs

# Going to work dir
cd /lustre/$USER/R-ARRAY

# Loading R module
module load R

# List files and get the "nth" file corresponding to ${SLURM_ARRAY_TASK_ID} with sed
R_FILE=$(ls -1 ${INPUT_DIR} | sed -n ${SLURM_ARRAY_TASK_ID}p)

# Downloading array.R if not present
if [[ ! -f array.R ]]; then
  wget https://forgemia.inra.fr/formationcalcul2022/formation-slurm/-/raw/main/Exercise8-SLURM_ARRAY/array.R
fi

# Launching arrays
Rscript array.R ${R_FILE}
```
Answers are in [array.bash](./array.bash)