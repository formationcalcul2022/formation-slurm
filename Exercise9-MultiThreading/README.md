# MultiThreading

A thread is a compute unit where a program can run. On the Meso@LR cluster, only 1 thread is available for one physical core (HyperThreading is OFF) which means that you can run only one program on a single core at a time.

Multi-threading allows a program or an application to use multiple cores for processing. For example, consider one of Muse's node (muse100) two Intel CPUs (Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz) with a total of 28 physical cores. Each of its cores has 1 thread which makes it a total of 28 thread so your program could use 28 threads to compute.

In SLURM, you will mainly have to play with these variables to run a multi-threaded job =>
```bash
-c n --cpus-per-tasks=n    # How many cores (threads) for a single program
-n N --nodes=N             # How many nodes for a single program
```

## Exercise single node multi-threading, openmp π 
In this exercise, we will compute π using a C code with OpenMP.

```bash
cd /lustre/$USER
mkdir pi-openmp

# Download the file in your scratch DIR
wget https://forgemia.inra.fr/formationcalcul2022/formation-slurm/-/raw/main/Exercise9-MultiThreading/openmp_pi.c
```

Compile it =>
```bash
# Compile hello_world_omp.c to hw_omp using openmp library -fopenmp...
gcc --std=c99 -fopenmp -o openmp_pi openmp_pi.c
```

Run it with "srun" command while playing with the "-c" option =>
```bash
# "./" is mandatory if you are on the same directory
# you can also use the absolute path to hw_omp /PATH/TO/hw_omp
srun -c 1 ./openmp_pi 1
srun -c 2 ./openmp_pi 2
srun -c 10 ./openmp_pi 10
```

What are your conclusions?

Now create a submit file to compile and launch "openmp_pi" program using 8 threads.

```bash
# Multi-threading =>
#SBATCH -N 1                                           # NUMBER OF NODES REQUESTED
#SBATCH -c 8                                           # NUMBER OF CORES FOR THE 
```
When working with multi-threaded jobs in "sbatch", the variable **OMP_NUM_THREADS** should always ben set to **$SLURM_CPUS_PER_TASK**. 
Reminder, $SLURM_CPUS_PER_TASK is the number of CPUs used for a single program (-c parameter).


While it is running, you can ssh the working node by doing the following commands to know what is going on =>
```bash
# where jobid is your job id, take notes of NODELIST
squeue -j $JOBID

# Get node number and ssh to the node
ssh muse111
# Type your password ...

# Launch this command on the node to monitor CPUs and memory in Linux
htop

# Exit by pressing the letter "q"
# Exit SSH shell
exit
```

## Exercise single node multi-threading MPI π 

Thanks: http://dominiquethiebaut.com/dftwiki3/index.php/Tutorial:_Computing_Pi_on_an_AWS_MPI-Cluster

Compile the file [mpi_pi.c](./mpi_pi.c) using these commands =>
```bash
mkdir -p /lustre/$USER/mpi-pi
cd /lustre/$USER/mpi-pi
wget https://forgemia.inra.fr/formationcalcul2022/formation-slurm/-/raw/main/Exercise9-MultiThreading/mpi_pi.c
module load cv-standard
module load openmpi
mpicc -o mpi_pi mpi_pi.c
```

Run it and play with the "-np" variable =>
```bash
time mpirun -np 1 mpi_pi 500000000
```

Time is a valuable tool to measure the time a process took to execute. It can be very usefull to check whether or not the parameter you are changing in SLURM are impacting your code performance.

You can read it like so =>
```bash
real    0m9.183s      # The time the machine took to run the whole program 
user    0m8.920s      # The time spent by the code itself on the CPU
sys     0m0.057s      # The time the kernel spent allocating stuf to the code (mainly accessing hardware)
```

It works the same way as our OpenMP code.
Now, make the batch script to run it on a single node using 8 cores.

I don't exactly know why, but you have to set "-oversubscribe" option for the code to work =>
```bash
time mpirun -np ${SLURM_CPUS_PER_TASK} -oversubscribe ./mpi_pi 5000000
```

## Exercise multiple node, multi-threading MPI + OpenMP
Using MESO@LR example here =>
```bash
mkdir -p /lustre/$USER/meso-examples
cd /lustre/$USER/meso-examples
wget https://hpc-lr.umontpellier.fr/wp-content/uploads/2017/01/slurm_muse_exemple.zip
unzip slurm_muse_exemple.zip
```

Build the mpi program in the directory "openmp-mpi" =>
```bash
cd exemples-slurm/openmp-mpi/
module purge
module load cv-standard
module load intel/compiler/64/2016.3.210
module load intel/mpi/64/2016.3.210

mpiicc -qopenmp -o ompi.x ompi.c
```

What is the number of cpus requested in "ompi.job" file?

You can compute it by doing "NODES * NTASKS * CPUS_PER_TASK" 

How many nodes will be used for the job?

You can launch it by using the file named "ompi.job" =>
```bash
sbatch ompi.job
```
