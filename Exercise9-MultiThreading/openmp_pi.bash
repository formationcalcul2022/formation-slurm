#! /usr/bin/env bash

# PARAMETERS

#SBATCH -J PI-OPENMP                                   # JOB NAME
#SBATCH -A inrae-infra                                 # GROUP/ACCOUNT NAME
#SBATCH -p defq                                        # PARTITION NAME
#SBATCH --output=/lustre/%u/logs/%x-%j.out             # OUTPUT FILE
#SBATCH --error=/lustre/%u/logs/%x-%j.err              # ERROR FILE
#SBATCH --mail-type=ALL                                # STATUS MAILED (BEGIN, FAILED, END)
#SBATCH --mail-user=jeremy.verrier@inrae.fr            # USER EMAIL  
#SBATCH --mem=1G                                       # MEMORY NEEDED FOR THE JOB
#SBATCH --time=01:00:00                                # TIME LIMIT

# Multi-threading =>
#SBATCH -N 1                                           # NUMBER OF NODES REQUESTED
#SBATCH -c 8                                           # NUMBER OF CORES FOR THE JOB

# MY COMMAND HERE =>

# cd to code
cd /lustre/$USER/pi-openmp

# compile 
gcc --std=c99 -fopenmp -o openmp_pi openmp_pi.c

# Launch program
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
./openmp_pi ${OMP_NUM_THREADS}