#! /usr/bin/env bash
# PARAMETERS

#SBATCH -J PI                                          # JOB NAME
#SBATCH -A inrae-infra                                 # GROUP/ACCOUNT NAME
#SBATCH -p defq                                        # PARTITION NAME
#SBATCH --output=/lustre/%u/logs/pi-%j.out             # OUTPUT FILE
#SBATCH --error=/lustre/%u/logs/pi-%j.err              # ERROR FILE
#SBATCH --mail-type=ALL                                # STATUS MAILED (BEGIN, FAILED, END)
#SBATCH --mail-user=prenom.nom@inrae.fr                # USER EMAIL
#SBATCH --mem=2M                                       # MEMORY NEEDED FOR THE JOB optimized
#SBATCH -c 1                                           # NUMBER OF CORES FOR THE JOB
#SBATCH --time=00:05:00                                # TIME LIMIT optimized

# MY COMMAND HERE =>
time echo "scale=10000; 4*a(1)" | bc -l

