#!/bin/sh 
#SBATCH --job-name=j2k_loire 
#SBATCH -t 02:00:00 
#SBATCH -o /lustre/%u/logs/j2k_loire-%A-%a.out 
#SBATCH -e /lustre/%u/logs/j2k_loire-%A-%a.err 
#SBATCH -p defq
#SBATCH -A U_RIVERLY 
#SBATCH --mail-type=ALL 
#SBATCH --mail-user=jeremie.bonneau@inrae.fr 

#SBATCH --mem=5G 
#SBATCH -n 1 
#SBATCH --array=1-1 
##SBATCH --array=1-100%50

# Loading modules...

module load jre/jre.8_x64

# ENV VAR

JAMS_PATH="/nfs/projets/JAMS" 
MODEL_PATH="JAMS-src/data/J2K_Loire" 
JAR_PATH="JAMS-src/jams-bin" 
JAR_BIN="jams-starter.jar"

# We want to get the ${SLURM_ARRAY_TASK_ID} "jam" file in the folder $MODEL_PATH =>
INPUT=$(find ${JAMS_PATH}/${MODEL_PATH} -name "*.jam" | grep -v "diag" | sed -n ${SLURM_ARRAY_TASK_ID}p) 
echo "Simulation du fichier ${INPUT}"

# launching jar

java -splash: -jar "${JAMS_PATH}"/"${JAR_PATH}"/"${JAR_BIN}" -n -m "${INPUT}"