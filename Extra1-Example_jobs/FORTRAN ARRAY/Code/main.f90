program GR1A

    ! Chargement des modules
    USE Data_TD
    USE Lecture
    USE Caracteristiques
    USE Ecriture
    USE Modele

    IMPLICIT NONE

    ! Déclaration
    CHARACTER(LEN = 256) :: sFicRes
    INTEGER i,iPar1,iPar2
    LOGICAL ok
    REAL, DIMENSION(3) :: MISC
    REAL, DIMENSION(2) :: Param,BestParam
    REAL, DIMENSION(2,1000) :: ParamTable
    REAL :: Crit,BestCrit

    ! Initialisation
    BestCrit = 1e8 ! Meilleur valeur de RMSE

    ! Appel de la subroutine LecConfig
    CALL LecConfig

    ! Appel de la subroutine LecData
    CALL LecData

    ! Appel de la subroutine Caract_BV
    CALL Caract_BV

    ! Paramètre à tester
    ParamTable(1,:)=REAL((/(i, i=1, 1000, 1)/))/100.
    ParamTable(2,:)=REAL((/(i, i=1, 1000, 1)/))/1000.

    ! Calage du modele
    WRITE(*,*)
    WRITE(*,'(A)') '## Calage du modele ##'
    ALLOCATE(QanSim(SIZE(Qan)))
    DO iPar1=1,size(ParamTable(1,:))
    DO iPar2=1,size(ParamTable(2,:))
        Param = (/ ParamTable(1,iPar1),ParamTable(2,iPar2) /)
        DO i = 2, SIZE(Qan)
            CALL MOD_GR1A(Param,Pan(i-1),Pan(i),ETPan(i),QanSim(i),MISC)
        END DO
        Crit = RMSE(QanSim(:),Qan(:))
        IF(Crit.LT.BestCrit) THEN
            BestParam = Param
            BestCrit = Crit
        ENDIF
    END DO
    END DO
    CALL SLEEP(10) ! Pour rendre le calcul plus lent
    WRITE(*,'(A25,2F8.2)') "Paramètre Optimisé: ",BestParam
    WRITE(*,'(A25,F8.2,X,A,F4.2,A)') "Critère Optimisé: ",BestCrit,"(",BestCrit/Qmmjmoy,")"

    ! Final run
    DO i = 2, SIZE(Qan)
      CALL MOD_GR1A(BestParam,Pan(i-1),Pan(i),ETPan(i),QanSim(i),MISC)
    END DO

    ! Visualisation des valeurs annuelles
    WRITE(*,*)
    WRITE(*,'(A)') '## Visualisation des resultats ##'
    WRITE(*,'(5A8)') "AN","P","ETP","Qobs","Qsim"
    DO i = 2, SIZE(Qan)
        WRITE(*,'(I8,4F8.2)') i,Pan(i),ETPan(i),Qan(i),QanSim(i)
    END DO

    ! Ecriture des valeurs annuelles
    WRITE(*,*)
    WRITE(*,'(A)') '## Ecriture des resultats ##'
    INQUIRE(file=TRIM(sCheminRes),exist=ok)
    IF(.NOT.ok) CALL SYSTEM('mkdir "'//TRIM(sCheminRes)//'"')
    sFicRes = TRIM(sCheminRes)//TRIM(sBV)//"_BV_Simulation.txt"
    CALL EcritSim(sFicRes)

end
