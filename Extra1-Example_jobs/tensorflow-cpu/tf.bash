#!/bin/bash

#SBATCH --account=E_RHAX
#SBATCH --exclusive=user
#SBATCH -N 1
#SBATCH -n 24
#SBATCH --error=/scratch/reyhaneh-sadat.ghazanfari-hashemi/%j.err
#SBATCH --job-name="s1_test"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=reyhaneh.hashemi@inrae.fr
#SBATCH --output=/scratch/reyhaneh-sadat.ghazanfari-hashemi/%j.out
#SBATCH --partition=q-irstea
#SBATCH --time=12:0:0
#SBATCH --array=5-566:24%10 

# Keep in mind that TF on CPU is bad.

module load python
cd /lustre/reyhaneh-sadat.ghazanfari-hashemi/
source activate REY 
conda info 
which python3
echo $SLURM_ARRAY_TASK_ID 

# Looping to assign each core to 1 tf jobs
for ((i=0; i<24; i++)) 
do 
        VAR=$(sed -n $((SLURM_ARRAY_TASK_ID + i))p 02_data/codes_bv_new.csv) 
        echo ${VAR} 
        if ! [ -z "${VAR}" ] 
        then 
                taskset -c ${i} ~/.conda/envs/REY/bin/python3.8 01_scripts/_runs_cluster_s1.py ${VAR}& 
                pids[${i}]=$! 
        else 
                echo "Plus rien à faire" 
        fi 
        unset VAR 
done 


# wait for all pids 
echo "WAITING" 
for pid in ${pids[*]}; do 
    wait $pid 
done