# My first job
Launching programs on SLURM means that you will launch "JOBS". Each jobs has a unique JOBID that allows you to track his progress. It is also used for accounting purposes.

## srun vs sbatch
They are two main ways to launch jobs :
* srun
* sbatch

We have to be very clear about the fact that they are not the same command. They each have their own options some are presents in both of them but not all. 

### srun

srun is mainly used to launch small programs without a lot of parameters. All the parameters have to be written in one line and can be a bit overwhelming =>
```bash
srun --job-name=R --nodes=1 --ntasks=1 --time=01:00:00  --pty --preserve-env bash -c 'module load system/R-3.6.0;R'
```
Also, if you close your terminal, the job stops.

The pro is that the result is in real-time, which means that all command outputs will appear on your screen. It can be usefull for a quick compilation or a test run of your code.

### sbatch

We strongly recommend you to use sbatch. sbatch needs a file that you have to make in order to run for example "my-first-job.sh" =>
```bash
#! /usr/bin/env bash

# PARAMETERS GOES HERE

#SBATCH -A f_meso    # GROUP
#SBATCH -p fmuse     # PARTITION

# MY COMMAND HERE =>
hostname
```
This file is composed of 3 parts:


* It must always start with the bash interpreter 

```bash
#! /usr/bin/env bash
```

 * The Slurm parameters must always begins with #SBATCH
```bash
#SBATCH 
```
* And finally, your commands must be placed at the end of the file. A list of commands can be launched, it is called a script. Those commands are going to be executed in the order you type them =>
```bash
# First command
cd DIRECTORY

# Second command 
ls

# Third command 
python run.py
```

Once the file done, you can launch it like so =>
```bash
sbatch my-first-job.sh
```

### Pros
* All the options are available 
* You don't have to let your terminal running in order to complete the job
* You can do advanced things like [ARRAYS](../Exercise8-SLURM_ARRAY) 

### Cons 
* The file can be tedious to make for small things like compiling or testing

# Exercise

## srun hostname
Connect to the cluster and launch those commands =>
```bash
hostname
srun hostname
```
What do you see? <br />
What does the ID means?



## sbatch hostname
Download the file [my-first-job.sh](./my-first-job.sh) and launch it on the cluster =>
```bash
git clone https://forgemia.inra.fr/formationcalcul2022/formation-slurm.git
cd formation-slurm/Exercise4-My_first_job/
sbatch my-first-job.sh
```
What happened? <br />
Let's check your output files to understand why it failed.
Where are they?<br />

Can you add a parameter to avoid losing those files? If so, which ones? (solution in [my-first-job-fixed.sh](./my-first-job-fixed.sh))<br />

### Editing a file in Linux

You can use the commands "nano" or "vi" to edit file. 
**Warning**, you can only use your arrow keys to navigate in a file, your mouse won't work in a terminal.

#### nano [CheatSheet](https://www.nano-editor.org/dist/latest/cheatsheet.html) =>
```bash
nano my-first-job.sh
... Edit your file ...
CTRL-X                   # CTRL-X exit the nano editor, 
y                        # if any changes has been made and you want to save them press "y", if not "n"
Enter                    # To commit changes in the current file
```
#### vi [CheatSheet](https://ryanstutorials.net/linuxtutorial/cheatsheetvi.php)=>
```bash
vi my-first-job.sh
INSERT                   # Enter edit mode, the "INSERT" key is located abose the "Delete" key on your keyboard
... Edit your file ...
ESCAPE                   # Stop insert mode
:w                       # Save the file
:q                       # Quit editor
:q!                      # force quit if stuck
```

#### Editing the file with MobaXTerm =>
![MobaXTerm editing a file](./mobaeditor.png?raw=true)

It can be very important to differentiate every output by giving them a unique name. If you don't do that, everytime you launch your job, it will rewrite the output files of the last one.

Whats does the "%j" and "%u" stand for in [my-first-job-fixed.sh](./my-first-job-fixed.sh)?

**Reminder** => [CheatSheet Slurm](https://nextcloud.inrae.fr/s/qs97RCJGF5tkMeK)

## Usefull commands
### Monitor your jobs
You can monitor the state of your job by launching this command =>
```bash
watch squeue -u $USER   # Launch the command "squeue -u verrierj" every 2 seconds
```
To exit "watch", use CTRL+C

### Prompt new lines in your output files

You can use the command "tail -f" to display the output file interactively. If a new line is added to your file, it will be prompted on the terminal if you launch =>

```bash
tail -f /lustre/$USER/logs/first.out
```

