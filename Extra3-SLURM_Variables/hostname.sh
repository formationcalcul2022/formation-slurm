#! /usr/bin/env bash

# PARAMETERS

#SBATCH -J hostname                                    # JOB NAME
#SBATCH -A inrae-infra                                 # GROUP/ACCOUNT NAME
#SBATCH -p defq                                        # PARTITION NAME
#SBATCH --output=/lustre/%u/logs/%x-%j.out             # OUTPUT FILE
#SBATCH --error=/lustre/%u/logs/%x-%j.err              # ERROR FILE
#SBATCH --mail-type=ALL                                # STATUS MAILED (BEGIN, FAILED, END)
#SBATCH --mail-user=prenom.nom@inrae.fr                # USER EMAIL  
#SBATCH --mem=1G                                       # MEMORY NEEDED FOR THE JOB
#SBATCH -c 1                                           # NUMBER OF CORES FOR THE JOB
#SBATCH --time=01:00:00                                # TIME LIMIT

# MY COMMAND HERE =>
hostname
echo $SLURM_CPUS_PER_TASK    # Number of CPUS per task (openmp)
echo $SLURM_JOB_ID           # JOBID   
echo $SLURM_JOB_ACCOUNT      # ACCOUNT (-A)
echo $SLURM_JOB_NAME         # JOBNAME (-J)
echo $SLURM_NODELIST         # On which nodes the job was computed
echo $SLURM_NTASKS           # How many tasks did it uses?